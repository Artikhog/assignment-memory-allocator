#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H

#include "../mem_utils/mem_internals.h"
#include "../mem_utils/mem.h"
#include "../mem_utils/util.h"
#include <stdio.h>
#include <unistd.h>

bool all_tests();

#endif