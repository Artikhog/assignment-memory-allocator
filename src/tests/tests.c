#include "tests.h"
#include <unistd.h>

bool common_allocation_test(void *heap) {
    fprintf(stdout, "Test 1 started. \n");
    void* block = _malloc(100);
    debug_heap(stdout, heap);
    if (block == NULL) {
        err("Test 1 failed: malloc return NULL\n");
        _free(block);
        return false;
    }
    else if (block_get_header(block)->capacity.bytes == 0) {
        err("Test 1 failed: malloc allocate zero capacity \n");
        _free(block);
        return false;
    }
    else if (block_get_header(block)->capacity.bytes != 100) {
        err("Test 1 failed: malloc allocate wrong capacity \n");
        _free(block);
        return false;
    }
    else if (block_get_header(block)->capacity.bytes == 100) {
        fprintf(stdout, "Test 1 passed \n");
        _free(block);
        return true;
    }
    return true;
}

bool free_one_block_test(void *heap) {
    fprintf(stdout, "Test 2 started. \n");
    void* block = _malloc(1000);
    _free(block);
    debug_heap(stdout, heap);
    if (!block_get_header(block)->is_free) {
        err("Test 2 failed: block is not free \n");
        return false;
    }  
    else if (block_get_header(block)->is_free) {
        fprintf(stdout, "Test 2 passed\n");
        return true;
    }     
    return true;
}

bool free_two_blocks_test(void *heap) {
    printf("Test 3 started. \n");
    void* block1 = _malloc(500);
    void* block2 = _malloc(100);
    debug_heap(stdout, heap);
    _free(block2);
    _free(block1);
    debug_heap(stdout, heap);
    if (!block_get_header(block1)->is_free && !block_get_header(block2)->is_free) {
        err("Test 3 failed: both blocks is not free\n");
        return false;
    } 
    else if (!block_get_header(block1)->is_free || !block_get_header(block2)->is_free) {
        err("Test 3 failed: one block is not free\n");
        return false;
    } 
    else if (block_get_header(block1)->is_free && block_get_header(block2)->is_free) {
        fprintf(stdout, "Test 3 passed\n");
        return true;
    }     
    return true;
}

bool grow_heap_test(void *heap) {
    fprintf(stdout, "Test 4 started. \n");
    debug_heap(stdout, heap);
    void* block1 = _malloc(500);
    void* block2 = _malloc(10001);
    if (block2 == NULL) {
        err("Test 4 failed: malloc return NULL\n");
        debug_heap(stdout, heap);
        _free(block1);
        _free(block2);
        return false;
    }
    if (block_get_header(block1)->capacity.bytes!=500 || block_get_header(block2)->capacity.bytes!=10001) { 
        err("Test 4 failed: malloc allocate wrong capacity\n");
        debug_heap(stdout, heap);
        _free(block1);
        _free(block2);
        return false;
    }
    if (block_get_header(block1)->capacity.bytes==500 && block_get_header(block2)->capacity.bytes==10001){
        fprintf(stdout,"Test 4 passed\n");
        debug_heap(stdout, heap);
        _free(block2);
        _free(block1);
        return true;
    }
    return true;
}

bool grow_heap_in_another_place_test(void *heap) {
    debug_heap(stdout, heap);
    fprintf(stdout, "Test 5 started. \n");
    struct block_header* heap_end = (struct block_header*)(heap);
    while (heap_end->next) {
        heap_end = heap_end->next;
    }
    heap_end += size_from_capacity(heap_end->capacity).bytes;
    mmap((void*) heap_end, 1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, 0, 0);
    debug_heap(stdout, heap);
    void* new_block = _malloc(40000);
    debug_heap(stdout, heap);
    struct block_header* block_header = block_get_header(new_block);
    if (block_header == heap_end) {
        _free(new_block);
        err("Test 5 failed: heap didn't grow in another region. \n");
        return false;
    }
    if (block_header != heap_end) {
        fprintf(stdout, "Test 5 passed. \n");
        _free(new_block);
        return true;
    }
    return true;
}

bool all_tests() {
    
    void *heap = heap_init(round_pages(2));

    if (!common_allocation_test(heap)) return false;

    if (!free_one_block_test(heap)) return false;
    
    if (!free_two_blocks_test(heap)) return false;

    if (!grow_heap_test(heap)) return false;

    if(!grow_heap_in_another_place_test(heap)) return false;

    fprintf(stdout, "All tests passed. \n");
    return true;
}

